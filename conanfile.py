from conan import ConanFile
from conan.tools.files import download
import os

class QtInstaller(ConanFile):
    name="qtinstaller"
    version="4.6.1"
    description="The Qt Installer Framework provides a set of tools and utilities to create installers for the supported desktop Qt platforms: Linux, Microsoft Windows, and macOS."
    url = "https://download.qt.io/official_releases/qt-installer-framework"
    homepage = "https://wiki.qt.io/Qt-Installer-Framework"
    license = "LGPLv3"
    no_copy_source = True
    short_paths = True
    settings={"os":["Windows", "Linux"]}

    def _installer(self) -> str : 
        if self.settings.os == "Linux":
            return "QtInstallerFramework-linux-x64-4.6.1.run"
        else :
            return "QtInstallerFramework-windows-x64-4.6.1.exe"

    def source(self):
        installer=self._installer()
        download(self, self.url + "/" + self.version + "/" + installer, installer)

    def package(self):
        installer=os.path.join(self.source_folder,self._installer())
        if self.settings.os == "Linux":
            self.run(f"chmod +x {installer}")
        self.run(f"{installer} --root {self.package_folder} --accept-licenses --default-answer --confirm-command install ")

    def package_info(self):
        self.buildenv_info.prepend_path("QTIFWDIR", self.package_folder)